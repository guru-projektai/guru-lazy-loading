<?php

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 */
class Guru_Lazy_Loading_Deactivator {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     */
    public static function deactivate() {}
}
