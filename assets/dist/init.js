document.addEventListener("DOMContentLoaded", function () {
  const lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazy:not(.skip-lz)",
    // threshold: -200,
    // callback_loading: function (el) {
    //
    // }
  });
  window.lazyLoadInstance = lazyLoadInstance;
  lazyLoadInstance.update();
});

window.addEventListener(
  "LazyLoad::Initialized",
  function (event) {
    window.lazyLoadInstance = event.detail.instance;
  },
  false
);
