<?php
/**
 * Plugin Name: Guru Lazy Loading
 * Description: Guru Lazy Loading
 * Version: 1.0.0
 * Author: Mindaugas
 */

if ( !defined( 'ABSPATH' ) ) {
    die;
}

define( 'Guru_Lazy_Loading_FILE', __FILE__ );
define( 'Guru_Lazy_Loading_PATH', plugin_dir_path( Guru_Lazy_Loading_FILE ) );
define( 'Guru_Lazy_Loading_URL', plugin_dir_url( Guru_Lazy_Loading_FILE ) );

register_deactivation_hook( Guru_Lazy_Loading_FILE, ['Guru_Lazy_Loading', 'guru_lazy_loading_deactivate'] );

final class Guru_Lazy_Loading
{
    /**
     * Plugin instance.
     *
     * @var Guru_Lazy_Loading
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return Guru_Lazy_Loading
     * @static
     */
    public static function get_instance()
    {
        if ( ! isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Guru_Lazy_Loading constructor.
     */
    private function __construct()
    {
        register_activation_hook(Guru_Lazy_Loading_FILE, [$this , 'guru_lazy_loading_activate']);

        $this->guru_lazy_loading_includes();

        add_action('wp_enqueue_scripts', [$this, 'enqueue']);

        add_action('init', [$this, 'guru_enable_filters'], PHP_INT_MAX);
    }

    /**
     * Enqueue Scripts and Styles
     */
    public function enqueue()
    {
        wp_enqueue_script('guru-lazy-loading-js', Guru_Lazy_Loading_URL . '/assets/dist/lazyload.min.js');
        wp_enqueue_script('guru-lazy-loading-init', Guru_Lazy_Loading_URL . '/assets/dist/init.js');
        wp_enqueue_style('guru-lazy-loading-css', Guru_Lazy_Loading_URL . '/assets/css/styles.css');
    }

    /**
     * Enables all filters required for lazy load to work
     */
    public function guru_enable_filters()
    {
        if (is_admin()) return;

        add_filter('the_content', [$this, 'guru_lazy_loading_content_images']);
        add_filter('widget_text', [$this, 'guru_lazy_loading_content_images']);
        add_filter('post_thumbnail_html', [$this, 'guru_lazy_loading_content_images']);
        add_filter('get_avatar', [$this, 'guru_lazy_loading_content_images']);
        add_filter('do_shortcode_tag', [$this, 'guru_lazy_loading_content_images']);
        add_filter('wp_get_attachment_image_attributes', [$this, 'guru_lazy_loading_attachments'], 10, 2);
    }

    /**
     * @param $content
     * @return string|string[]|null
     *
     * Filters out and adds classes for lazy load to work
     */
    public function guru_lazy_loading_content_images($content)
    {
        // Remove from content noscript (no affets on noscript tags)
        $content = preg_replace( '/<noscript.*?(\/noscript>)/i', '', $content );

        $content = mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8');

        // Reset
        $content = str_replace(['lazy', 'data-src'], [' ', 'src'], $content);

        // Add lazy class
        $pattern = '/<img(.*?)class=\"(.*?)\"(.*?)>/i';
        $replacement = '<img$1class="$2 lazy"$3>';
        $content = preg_replace($pattern, $replacement, $content);

        // Set alt="example" to alt
//        $pattern ="/<img(.*?)alt=\"(.*?)\"(.*?)>/i";
//        $replacement = '<img$1alt$3>';
//        $content = preg_replace($pattern, $replacement, $content);

        // Remove attributes
        $content = preg_replace('/sizes=\\"[^\\"]*\\"/', '', $content);
        $content = preg_replace('/srcset=\\"[^\\"]*\\"/', '', $content);

        // Replace src with data-src
        $content = preg_replace( '/(<img.+)(src)/Ui', '$1data-src', $content );

        // Remove specific classes
        $content = str_replace('so-widget-image', ' ', $content);

        // Replace DEV and Staging URLs to PROD.
        $content = str_replace('scg.lndo.site/app/uploads/', 'smartcasinoguide.com/app/uploads/', $content);
        // $content = str_replace('staging.smartcasinoguide.com/app/uploads/', 'smartcasinoguide.com/app/uploads/', $content);
        $content = str_replace('sbg.lndo.site/app/uploads/', 'smartbettingguide.com/app/uploads/', $content);
        $content = str_replace('staging.smartbettingguide.com/app/uploads/', 'smartbettingguide.com/app/uploads/', $content);

        // Replace background(-image) style with lazy-bg tag
        preg_match_all('~\bstyle=(\'|")(.*?)background(-image)?\s*:(.*?)\(\s*(\'|")?(?<image>.*?)\3?\s*\)~i',$content,$matches);

        foreach( $matches[0] as $match ){
            preg_match('~\bbackground(-image)?\s*:(.*?)\(\s*(\'|")?(?<image>.*?)\3?\s*\)~i',$match,$bg);
            $bg_less_match = str_replace( $bg[0], '', $match );
            $data_match = 'data-bg="'.$bg['image'].'" '.$bg_less_match;
            $content = str_replace( [$match.';', $match], [$data_match, $data_match], $content);
        }

        // Data-bg add class
        preg_match_all('(<(.*?)data-bg="(.*?)">)', $content, $cssBackgroundTags);

        foreach ($cssBackgroundTags[0] as $bg) {
            $document = new DOMDocument();
            $document->loadHTML($bg);

            $xpath = new DOMXPath($document);
            $results = $xpath->query("//*[@data-bg]");

            foreach($results as $node) {
                $oldClass = $node->getAttribute('class');
                $node->setAttribute('class', $oldClass . ' lazy');

                $content = str_replace($bg, $document->saveXML($node), $content);
            }
        }

        // Add class attribute if doesnt have
        preg_match_all('(<img(.*?)data-src="(.*?)"(.*?)>)', $content, $everyImgWithDataSrc);

        foreach ($everyImgWithDataSrc[0] as $img) {
            if (strpos($img, 'class=') === false) {
                $document = new DOMDocument();
                $document->loadHTML($img);

                $imgs = $document->getElementsByTagName('img');

                foreach ($imgs as $item) {
                    $item->setAttribute('class', 'lazy');

                    $content = str_replace($img, $document->saveXML($item), $content);
                }

            }
        }

        // Set placeholder
        preg_match_all('(<img(.*?)data-src="(.*?)"(.*?)>)', $content, $filteredImgs);

        foreach ($filteredImgs[0] as $img ) {
            $document = new DOMDocument();
            $document->loadHTML($img);

            $lazyloadTags = $document->getElementsByTagName('img');

            foreach ($lazyloadTags as $tag) {
                // Skip lazyloading with class (skip-lz)
                $classes = $tag->getAttribute('class');

                if (strpos($classes, 'skip-lz') !== false) {
                    $tag->setAttribute('src', $tag->getAttribute('data-src'));
                    $tag->removeAttribute('data-src');
                    $tag->setAttribute('class', str_replace('lazy', ' ', $classes));
                    $content = str_replace($img, $document->saveXML($tag), $content);
                    continue;
                }

                $width = $tag->getAttribute('width');
                $height = $tag->getAttribute('height');

                if (! $height || ! $width) continue;

                $tag->setAttribute('src', 'data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ' . $width . ' ' . $height .'"%3E%3C/svg%3E');

                // Create noscript
                $noscript = $document->createElement('noscript');
                $node = $document->createElement('img');
                $node->setAttribute('src', $tag->getAttribute('data-src'));

                $noscript->appendChild($node);

                $tag->appendChild($noscript);

                $content = str_replace($img, $document->saveXML($tag), $content);
            }
        }

        return $content;
    }

    /**
     * @param $atts
     * @param $attachment
     * @return mixed
     *
     * Adds attributes to img tags for lazy load to work
     */
    public function guru_lazy_loading_attachments($atts, $attachment)
    {
        if (! isset($atts['data-src'])) {
            $atts['data-src'] = $atts['src'];
        }

//        $metadata = wp_get_attachment_metadata($attachment->ID);

//        if (strpos($atts['src'], 'data:image/svg+xml') === false) {
//            $atts['src'] = 'data:image/svg+xml,%3Csvg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 '. $metadata['width'] . ' ' . $metadata['height'] .'"%3E%3C/svg%3E';
//        } else {
            unset($atts['src']);
//        }

        if( isset( $atts['srcset'] ) ) {
            unset( $atts['srcset'] );
        }

        if (isset($atts['sizes'])) {
            unset( $atts['sizes']);
        }

//        if (isset($atts['alt'])) {
//            $atts['alt'] = '';
//        }

        if (isset($atts['class'])) {
            if (strpos($atts['class'], 'lazy') === false) {
                $atts['class'] .= ' lazy';
            }
        } else {
            $atts['class'] = 'lazy';
        }

        return $atts;
    }


    /**
     * Run when deactivate plugin.
     */
    public static function guru_lazy_loading_deactivate()
    {
        require_once Guru_Lazy_Loading_PATH . 'includes/guru-lazy-loading-deactivator.php';
        Guru_Lazy_Loading_Deactivator::deactivate();
    }

    /**
     * Run when activate plugin.
     */
    public function guru_lazy_loading_activate()
    {
        require_once Guru_Lazy_Loading_PATH . 'includes/guru-lazy-loading-activator.php';
        Guru_Lazy_Loading_Activator::activate();
    }

    /**
     * Loading plugin functions files
     */
    public function guru_lazy_loading_includes()
    {
        require_once Guru_Lazy_Loading_PATH . 'includes/guru-lazy-loading-functions.php';
    }
}

function Guru_Lazy_Loading()
{
    return Guru_Lazy_Loading::get_instance();
}

$GLOBALS['Guru_Lazy_Loading'] = Guru_Lazy_Loading();
